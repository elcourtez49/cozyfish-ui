# Listes des changements et images d'exemple

## Esthétique et accessibilité
- Modification du border-radius
- Redesign du player audio et vidéo
- Redesign des séparateurs de dates
- Redesign des inputs (champs de texte, switch, ...)
- Redesign des boutons "Danger" (supprimer, deconnecter, ...)
- Redesign de la bannière de profil
- Redesign des publications boostées
- Redesign des publications 
- Redesign des content warnings
- Redesign des annonces
- Redesign des boutons de réactions
- Redesign des hashtags, liens et mentions
- Redesign des publications silenciées
- Changements de certains icônes
- Vrai thème opaque
- Redesign des onglets "Publications", "Publications et réponses" du profil
- Couleur flat pour les boutons de publication
- Cache les up arrows dans les threads 
- Ajustement de certains éléments dans l'onglet notification (widget et section)
- Ajoute une bordure à gauche de la timeline
- Ajustement de la barre des fenêtres
- Ajustement de la taille des fonts (de manière général, tout est plus gros)
- Redesign des labels d'instances sur les publications
- Suppression de la bannière de profil dans la navbar
- Bouton de publication centré
- L'indicateur de notification est statique
- Ajustement des coins des boutons de la navbar

## Bugs
- Possibilité de redimensionné les champs de texte
- Résolution de bugs dans la page des réactions sur le profil
- Résolution des apparitions de soulignement dans les boutons de la navbar
- Résolution du clignotement du pouton "Publier" de la navbar
- La première publications de la TL n'est plus coupée
- Ajustement de la ligne de réponse dans les notifications
- Ajustement de l'alignement vertical des icônes
- Modification des menus dépliants dans la section modération
- Changement de couleur hover dans le sélecteur d'emoji
- Ajustement du masque dans les pages avec plusieurs onglets (on voyait l'ombre de l'onglet suivant)

## Features
- Cache en partie les publications parents d'une réponse
- Cache le bouton "Boost" des publications sans texte alternatif
- Cache nos propres publications de la TL
- Cache les indicateurs d'activités (sauf "En ligne")







