## Cozy redesign for Firefish's Web UI 🐙  
CozyFish UI is a more accessible and cozyiest themes and custom CSS for Firefish. Warm and cozy colors, more bigger icons and fonts, and more.

[📢 **Announcement** post on Firefish](--)

## Summary

* [**Installation**](#installation)
  * [Install the custom CSS](#installation)
  * [Install the theme](#installation-of-the-theme)
* [**Things to know**](#things-to-know)
* [**Accessibility**](#accessibility)
* [**Contributions**](#contribution)

## Installation

### Installation of the custom CSS

**🛑 WARNING** Some elements of the CSS work only with French interfaces. I work to use classes usable by all languages 

Steps if you want to install the custom CSS (or a part of it):

1. Go to Settings > Theme > Custom CSS
2. Copy and paste the [code](https://gitlab.com/minybol/firefish-customisation/-/blob/main/firefish-customcss.css?ref_type=heads) on the text area
3. Click "Save" and refresh the page

If you're using Firefox, you need to activate the has() CSS selector :
Go to `about:config` page, then search and toggle `layout.css.has-selector.enabled`

### Installation of the theme
Steps if you want to install my themes: 

1. Go to Settings > Theme > Install a theme
2. Copy and paste the code of the [light](https://gitlab.com/minybol/firefish-customisation/-/blob/main/cozy-theme-light.js?ref_type=heads) or [dark](https://gitlab.com/minybol/firefish-customisation/-/blob/main/cozy-theme-dark.js?ref_type=heads) mode in the text area
3. Click "Install" 
4. Go again if Theme and choose it in the theme picker 

## Things to know
* **CozyFish UI's custom CSS may have issue with some themes**. The UI can't be accessible with all the existing themes because of certain color variables
* **The code can be dirty**. I’m not a developer and I do this for fun. You can make pull requests to clean what I do or add features

## Accessibility
Current themes are not entirely intended for people with eye impairments. I planned to make a dark and clear theme with higher contrasts

## Contribution 
- [Brome](https://social.marud.fr/@brome)